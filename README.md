# Notes About This Site

## Language of the Site

The site seems to be written in plain HTML

## Migration Notes

No special needs, everything should be straightforward.

## Website Location

Hosted on gitlab pages.