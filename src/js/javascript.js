var currentPage = [0];
var resetTimer;
var topicsTimer;

function navOver(a) {
	clearTimeout(resetTimer);
	var navIndexes = (a == null) ? [] : a.id.split("navLink").join("").split("_");
	
	//nav0 loop
	for (x=0; x<=7; x++) {
		if(document.getElementById("navItem" + x)) {
			document.getElementById("navItem" + x).className = (navIndexes[0] == x) ? "nav0ItemOn" : "nav0ItemOff";
			imageBaseName = document.getElementById("navImage" + x).src.split(".gif").join("").split("_on").join("");
			document.getElementById("navImage" + x).src = imageBaseName + ((navIndexes[0] == x) ? "_on.gif" : ".gif");
		}
	}
	
	//nav1 swap
	for (x=0; x<=7; x++)
		if(document.getElementById("nav1_" + x)) 
			document.getElementById("nav1_" + x).style.display = (navIndexes[0] == x) ? "block" : "none";
			
	//nav1 loop
	for (x=0; x<=7; x++)
		for (y=0; y<=7; y++)
			if(document.getElementById("navItem" + x + "_" + y)) {
				isOn = false;
				if (navIndexes.length >= 2) if (navIndexes[0] == x && navIndexes[1] == y) isOn = true;
				document.getElementById("navItem" + x + "_" + y).className = (isOn) ? "nav1ItemOn" : "nav1ItemOff";
				imageBaseName = document.getElementById("navImage" + x + "_" + y).src.split(".gif").join("").split("_on").join("");
				document.getElementById("navImage" + x + "_" + y).src = imageBaseName + ((isOn) ? "_on.gif" : ".gif");
			}
				
	//nav2 swap
	for (x=0; x<=7; x++)
		for (y=0; y<=7; y++)
			if(document.getElementById("nav2_" + x + "_" + y)) {
				displayFlag = "none";
				if (navIndexes.length >= 2)
					if (navIndexes[0] == x && navIndexes[1] == y) displayFlag = "block";
				document.getElementById("nav2_" + x + "_" + y).style.display = displayFlag;
			}

	//nav2 loop
	for (x=0; x<=7; x++)
		for (y=0; y<=7; y++)
			for (z=0; z<=7; z++)
				if(document.getElementById("navItem" + x + "_" + y + "_" + z)) {
					isOn = false;
					if (navIndexes.length >= 3) if (navIndexes[0] == x && navIndexes[1] == y && navIndexes[2] == z) isOn = true;
					document.getElementById("navItem" + x + "_" + y + "_" + z).className = (isOn) ? "nav2ItemOn" : "nav2ItemOff";
					imageBaseName = document.getElementById("navImage" + x + "_" + y + "_" + z).src.split(".gif").join("").split("_on").join("");
					document.getElementById("navImage" + x + "_" + y + "_" + z).src = imageBaseName + ((isOn) ? "_on.gif" : ".gif");
				}

}

function navOut() {
	resetTimer = setTimeout("navReset()",500);
}

function navReset() {
	navOver(document.getElementById("navLink" + currentPage.join("_")));
}

function over(a, on) {
	var linkTxt = new RegExp("Link");
	var img = document.getElementById(a.id.replace(linkTxt, "Img"));
	var imageBaseName = img.src.split(".gif").join("").split("_on").join("");
	img.src = imageBaseName + ((on) ? "_on.gif" : ".gif");
}

var jsnavIndex = [0,10,20,30,40,50,60,70];

function jsnavOver(a, on) {
	var index = parseInt(a.id.split("jsnavLink").join(""));
	var set = Math.floor(index / 10);
	index = index % 10;
	var img;
	var imageBaseName;
	for (x=set*10 + 0; x<=set*10 + 7; x++) {
		img = document.getElementById("jsnavImg" + x);
		if (img) {
			imageBaseName = img.src.split(".gif").join("").split("_on").join("");
			img.src = imageBaseName + (((on && x== set*10 + index) || (!on && x==jsnavIndex[set])) ? "_on.gif" : ".gif");
		}
	}
}

function jsnavSelect(a) {
	var index = parseInt(a.id.split("jsnavLink").join(""));
	var set = Math.floor(index / 10);
	jsnavIndex[set] = index;
	index = index % 10;
	for (x=set*10 + 0; x<=set*10 + 7; x++)
		if (document.getElementById("jsnavContent" + x))
			document.getElementById("jsnavContent" + x).style.display = (x == set*10 + index) ? "block" : "none";
	jsnavOver(a, false);
}


function jsnavSelect2(a, id) {
	var index = parseInt(a.id.split("jsnavLink").join(""));
	var set = Math.floor(index / 10);
	jsnavIndex[set] = index;
	index = index % 10;
	for (x=set*10 + 0; x<=set*10 + 7; x++)
		if ($("jsnavContent" + x))
			$("jsnavContent" + x).style.display = (x == set*10 + index) ? "block" : "none";
	jsnavOver(a, false);
}

function showTopics(on) {
	a = new Object();
	a.id = "jsnavDropdownLink";
	if (on) {
		clearTimeout(topicsTimer);
		document.getElementById("faqTopics").style.display = "block";
		over(a, true);
	} else {
		topicsTimer = setTimeout("resetTopics()",500);
	}
}

function resetTopics() {
	clearTimeout(topicsTimer);
	over(a, false);
	document.getElementById("faqTopics").style.display = "none";
}

function resetTopics2(id) {
	clearTimeout(topicsTimer);
	over(a, false);
	$(id).style.display = "none";
}

function showTopics2(on, id) {
	a = new Object();
	a.id = "jsnavDropdownLink";
	if (on) {
		clearTimeout(topicsTimer);
		$(id).style.display = "block";
		over(a, true);
	} else {
		topicsTimer = setTimeout("resetTopics2("+id+")",500);
	}
}
var mediaCenterNewsIndex = 0;
var mediaCenterNewsCount = 5;

function showMediaCenterNews(a) {
	var index = parseInt(a.id.split("mediaCenterNewsLink").join(""));
	for (x=0; x<mediaCenterNewsCount; x++) {
		if(document.getElementById("mediaCenterNews" + x)) document.getElementById("mediaCenterNews" + x).style.display = (x == index) ? "block" : "none";
		img = document.getElementById("mediaCenterNewsImg" + x);
		if(img) {
			imageBaseName = img.src.split(".gif").join("").split("_on").join("");
			img.src = imageBaseName + ((x==index) ? "_on.gif" : ".gif");
		}
	}
	mediaCenterNewsIndex = index;
}

function scrollMediaCenterNews(increment) {
	index = mediaCenterNewsIndex + increment;
	if (index >= mediaCenterNewsCount) index = 0;
	if (index < 0) index = mediaCenterNewsCount - 1;
	a = new Object();
	a.id = "mediaCenterNewsLink" + index;
	showMediaCenterNews(a);
}

function navToSelection(idName) {
	toUrl = $(idName).value;
	if (toUrl.indexOf("http://") > -1) {
		var vars = "width=800,height=600";
		if (toUrl.indexOf("::") > -1) {
			vars = toUrl.split("::")[1];
			toUrl = toUrl.split("::")[0];
		}
		window.open(toUrl, 'child', vars);
	} else if (toUrl) {
		window.location = toUrl;
	}
}

 function popWindow(url) {
 	//winLeft = (screen.width-500)/2;
 	//winTop = (screen.height-(360))/2; 
	myLeft=(screen.width)?(screen.width-500)/2:100;
	myTop=(screen.height)?(screen.height-270)/2:100;
	winOptions = 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1,width=500,height=270,top='+myTop+',left='+myLeft;
	var temp = window.open(url,'popWin',winOptions);
	temp.focus();
}

 function openWin(url, width, height) {
 	//winLeft = (screen.width-500)/2;
 	//winTop = (screen.height-(360))/2; 
	myLeft=(screen.width)?(screen.width-width)/2:100;
	myTop=(screen.height)?(screen.height-height)/2:100;
	winOptions = 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=1,width=' + width + ',height=' + height +',top='+myTop+',left='+myLeft;
	var temp = window.open(url,'popWin',winOptions);
	temp.focus();
}
